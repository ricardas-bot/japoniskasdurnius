package com.exappex.japoniskasdurnius;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements DialogInterface.OnClickListener, View.OnClickListener {

    ImageView playerDeck, playerCard1, playerCard2, playerCard3, playerCard4, playerCard5, playerCard6, playerCard7, playerCard8, playingCard1;




    ArrayList<Integer> cards;
    ArrayList<ImageView> player1CardsInHand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playerDeck = (ImageView) findViewById(R.id.deck);
        playerCard1 = (ImageView) findViewById(R.id.playerCard1);
        playerCard2 = (ImageView) findViewById(R.id.playerCard2);
        playerCard3 = (ImageView) findViewById(R.id.playerCard3);
        playerCard4 = (ImageView) findViewById(R.id.playerCard4);
        playerCard5 = (ImageView) findViewById(R.id.playerCard5);
        playerCard6 = (ImageView) findViewById(R.id.playerCard6);
        playerCard7 = (ImageView) findViewById(R.id.playerCard7);
        playerCard8 = (ImageView) findViewById(R.id.playerCard8);

//        playingCard1 = (ImageView) findViewById(R.id.playingCard1);

        playerCard1.setVisibility(View.INVISIBLE);
        playerCard2.setVisibility(View.INVISIBLE);
        playerCard3.setVisibility(View.INVISIBLE);
        playerCard4.setVisibility(View.INVISIBLE);
        playerCard5.setVisibility(View.INVISIBLE);
        playerCard6.setVisibility(View.INVISIBLE);
        playerCard7.setVisibility(View.INVISIBLE);
        playerCard8.setVisibility(View.INVISIBLE);

       player1CardsInHand = new ArrayList<>();
       player1CardsInHand.add(playerCard1);
        player1CardsInHand.add(playerCard2);
        player1CardsInHand.add(playerCard3);
        player1CardsInHand.add(playerCard4);


        //Added on click listeners to player cards in hand
        for(int i = 0; i < player1CardsInHand.size(); i++) {
//            player1CardsInHand.get(i).setOnClickListener(this);
            final int finalI = i;
            player1CardsInHand.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    putCardToPlay(player1CardsInHand.get(finalI));
                    Log.i("myTag", "This is my message");
                }
            });
        }


        cards = new ArrayList<>();

         for(int i = 0; i < 9; i++) {
             cards.add(i);
         }


         playerDeck.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Collections.shuffle(cards);
                 setImages(cards.get(0), playerCard1);
                 setImages(cards.get(1), playerCard2);
                 setImages(cards.get(2), playerCard3);
                 setImages(cards.get(3), playerCard4);
                 setImages(cards.get(4), playerCard5);
                 setImages(cards.get(5), playerCard6);
                 setImages(cards.get(6), playerCard7);
                 setImages(cards.get(7), playerCard8);

                 playerCard1.setVisibility(View.VISIBLE);
                 playerCard2.setVisibility(View.VISIBLE);
                 playerCard3.setVisibility(View.VISIBLE);
                 playerCard4.setVisibility(View.VISIBLE);
                 playerCard5.setVisibility(View.VISIBLE);
                 playerCard6.setVisibility(View.VISIBLE);
                 playerCard7.setVisibility(View.VISIBLE);
                 playerCard8.setVisibility(View.VISIBLE);

             }


         });

        while (true) {
            // code block to be executed

            break;
        }
         playerCard1.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 putCardToPlay(playerCard1);
                 Log.i("myTag", "This is my message");
             }
         });

        }

    public void putCardToPlay(ImageView card) {
        LinearLayout playingCards = (LinearLayout) findViewById(R.id.playingCards);
        LinearLayout player1Cards = (LinearLayout) findViewById(R.id.playerCards);

        player1Cards.removeView(card);
        playingCards.addView(card);

        player1Cards.invalidate();
        playingCards.invalidate();
    }

    public void setImages(int card, ImageView image) {
        switch (card) {
            case 0:
                image.setImageResource(R.drawable.clubs2);
                break;
            case 1:
                image.setImageResource(R.drawable.clubs3);
                break;
            case 2:
                image.setImageResource(R.drawable.diamonds2);
                break;
            case 3:
                image.setImageResource(R.drawable.spades_ace);
                break;
            case 4:
                image.setImageResource(R.drawable.hearts_jack);
                break;
            case 5:
                image.setImageResource(R.drawable.clubs5);
                break;
            case 6:
                image.setImageResource(R.drawable.spades2);
                break;
            case 7:
                image.setImageResource(R.drawable.spades3);
                break;
            case 8:
                image.setImageResource(R.drawable.spades4);
                break;
            case 9:
                image.setImageResource(R.drawable.spades5);
                break;

        }



    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

    }

    @Override
    public void onClick(View view) {

    }
}
